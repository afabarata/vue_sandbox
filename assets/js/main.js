class Movie {
    constructor(title, year, genre, director, poster, imdb) {
        this.title = title;
        this.year = year;
        this.genre = genre;
        this.director = director;
        this.poster = poster;
        this.imdb = imdb;
    }
};

const moviesArray = [
    new Movie(
        'The Shawshank Redemption',
        '1994',
        'Crime, Drama',
        'Frank Darabont',
        'https://images-na.ssl-images-amazon.com/images/M/MV5BODU4MjU4NjIwNl5BMl5BanBnXkFtZTgwMDU2MjEyMDE@._V1_UX182_CR0,0,182,268_AL_.jpg',
        'http://www.imdb.com/title/tt0111161/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2398042102&pf_rd_r=0722P11XKHW7Q9550SXT&pf_rd_s=center-1&pf_rd_t=15506&pf_rd_i=top&ref_=chttp_tt_1'
    ),
    new Movie(
        'The Godfather',
        '1972',
        'Crime, Drama',
        'Francis Ford Coppola',
        'https://images-na.ssl-images-amazon.com/images/M/MV5BZTRmNjQ1ZDYtNDgzMy00OGE0LWE4N2YtNTkzNWQ5ZDhlNGJmL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY268_CR3,0,182,268_AL_.jpg',
        'http://www.imdb.com/title/tt0068646/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2398042102&pf_rd_r=0722P11XKHW7Q9550SXT&pf_rd_s=center-1&pf_rd_t=15506&pf_rd_i=top&ref_=chttp_tt_2'
    ),
    new Movie(
        'The Godfather: Part II',
        '1974',
        'Crime, Drama',
        'Frank Darabont',
        'https://images-na.ssl-images-amazon.com/images/M/MV5BMjZiNzIxNTQtNDc5Zi00YWY1LThkMTctMDgzYjY4YjI1YmQyL2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UY268_CR3,0,182,268_AL_.jpg',
        'http://www.imdb.com/title/tt0071562/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2398042102&pf_rd_r=0722P11XKHW7Q9550SXT&pf_rd_s=center-1&pf_rd_t=15506&pf_rd_i=top&ref_=chttp_tt_3'
    ),
    new Movie(
        'The Dark Knight',
        '2008',
        'Action, Crime, Drama',
        'Christopher Nolan',
        'https://images-na.ssl-images-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_UX182_CR0,0,182,268_AL_.jpg',
        'http://www.imdb.com/title/tt0468569/?pf_rd_m=A2FGELUUNOQJNL&pf_rd_p=2398042102&pf_rd_r=0722P11XKHW7Q9550SXT&pf_rd_s=center-1&pf_rd_t=15506&pf_rd_i=top&ref_=chttp_tt_4'
    ),
];

const productsArray = [
    {
        'id': 1,
        'name': 'Pomegranate',
        'image': 'https://cdn.grofers.com/app/images/products/normal/pro_365530.jpg',
        'price': 1,
        'amount': 1
    },
    {
        'id': 2,
        'name': 'Apple',
        'image': 'https://img.coopathome.ch/produkte/250_250/RGB/3597652_001.jpg?_=1504518428998',
        'price': 1.5,
        'amount': 1
    },
    {
        'id': 3,
        'name': 'Passion Fruit',
        'image': 'https://4.imimg.com/data4/HP/MS/MY-15574058/passion-fruit-250x250.jpg',
        'price': 2,
        'amount': 1
    },
];

function existOnArray(shoppingList, item) {
    for(let i = 0; i < shoppingList.length; i++) {
        if(shoppingList[i].id === item.id) {
            shoppingList[i].amount++;
            return true;
        }
    }
    return false;
}

const app = new Vue({
    el: '#sandbox',
    data: {
        title: 'Vue Sandbox',
        TitColorGreen: true,
        moviesEnable: true,
        movieKeyword: '',
        movies: moviesArray,
        shoppingCartEnable: false,
        products: productsArray,
        shoppingList: [],
        shoppingCartItems: 0,
        arraySortingEnable: false,
        arraySortingDescActive: false,
        arraySortingInvertedActive: false,
        numberSortingInput: 0,
        arraySorting: [],
    },
    methods: {
        changeTitColor() {
            this.TitColorGreen = !this.TitColorGreen;
        },
        movieListToggle() {
            this.moviesEnable = true;
            this.shoppingCartEnable = false;
            this.arraySortingEnable = false;
        },
        findNumberToggle() {
            this.moviesEnable = false;
            this.shoppingCartEnable = true;
            this.arraySortingEnable = false;
        },
        arraySortingToggle() {
            this.moviesEnable = false;
            this.shoppingCartEnable = false;
            this.arraySortingEnable = true;
        },
        addToShoppingCart(item){
            this.shoppingCartItems++;
            if(!existOnArray(this.shoppingList, item)) {
                this.shoppingList.push(item);
            }
        },
        addToSortingArray() {
            if(!this.arraySortingDescActive && !this.arraySortingInvertedActive) {
                this.arraySorting.push(this.numberSortingInput);
            }
            if(this.arraySortingDescActive) {
                this.arraySorting.push(this.numberSortingInput);
                this.arraySortingDesc();
            }
            if(this.arraySortingInvertedActive) {
                this.arraySorting.unshift(this.numberSortingInput);
            }
        },
        arraySortingDesc() {
            this.arraySortingDescActive = true;
            this.arraySortingInvertedActive = false;

            let arrayAux = [];
            let indexAux = 0;
            while(this.arraySorting.length > 0) {
                let elemAux = -Infinity;
                for(let i = 0; i < this.arraySorting.length; i++) {
                    /*if(i === 0) {
                        elemAux = this.arraySorting[i];
                    }*/
                    if(parseFloat(elemAux) < parseFloat(this.arraySorting[i])) {
                        elemAux = this.arraySorting[i];
                        indexAux = i;
                    }
                    if(parseFloat(elemAux) === parseFloat(this.arraySorting[i])) {
                        indexAux = i;
                    }
                }
                if (indexAux > -1) {
                    this.arraySorting.splice(indexAux, 1);
                }
                arrayAux.push(elemAux);
            }
            this.arraySorting = arrayAux;
        },
        arraySortingInverted() {
            this.arraySortingDescActive = false;
            this.arraySortingInvertedActive = true;

            let arrayAux = [];
            for(let i = this.arraySorting.length-1; i >= 0; i--) {
                arrayAux.push(this.arraySorting[i]);
            }
            this.arraySorting = arrayAux;
        }
    },
    computed: {
      filteredMovieList(){
          return this.movies.filter((movie) =>{
              return movie.title.toLocaleLowerCase().includes(this.movieKeyword.toLocaleLowerCase());
          })
      },
      shoppingCartPrice (){
          let totalPrice = 0;
          for(let i = 0; i < this.shoppingList.length; i++) {
                totalPrice+= (this.shoppingList[i].amount * this.shoppingList[i].price);
          }
          return totalPrice;
      }
    }
});